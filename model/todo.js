const { Schema, model, SchemaTypes: { Mixed } } = require('mongoose');
const { userSchema } = require('./user');

const todoSchema = new Schema({
    name: String,
    owner: userSchema,
    items: Mixed
});

const todoModel = model('todo', todoSchema);

module.exports = {
    todoModel,
    todoSchema
}