const { Schema, model } = require('mongoose');

const userSchema = new Schema({
    login: String,
    password: String,
    dateOfReg: { type: Date, default: new Date() }
});
const userModel = model('users', userSchema);

module.exports = {
    userSchema,
    userModel
}