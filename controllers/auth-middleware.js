const { userModel: users } = require('../model/user');

async function middleware(req, res, next) {
    const header = req.header('X-AUTH-TOKEN');
    //const header = req.cookies['X-AUTH-TOKEN'];
    if (header === null) {
        return res.json({
            error: "Not authorized"
        });
    }
    const user = await users.findById(header);
    if (user === null) {
        return res.json({
            error: "Not authorized"
        });
    }
    req.user = user;
    next();
}

module.exports = {
    middleware
}