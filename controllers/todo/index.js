const router = require('express').Router();
const { todoModel: todos } = require('../../model/todo');
const { middleware } = require('../auth-middleware');
router.use(middleware);

// LIST
router.get('/', async (req, res) => {
    const list = await todos.find({ "owner._id": req.user._id });
    return res.json({
        result: list
    });
});

// GET BY ID
router.get('/:id', async (req, res) => {
    const one = await todos.findById(req.params.id);
    return res.json({
        result: one
    });
});

// CREATE
router.post('/', async (req, res) => {
    const { name } = req.body;
    const saved = await todos.create({
        name: name,
        items: [],
        owner: req.user
    });
    return res.json({
        result: saved
    });
});

// UPDATE
router.post('/:id', async (req, res) => {
    const { name } = req.body;
    const updated = await todos.findOneAndUpdate({ _id: req.params.id, 'owner._id': req.user._id }, { $set: { name: name }}, { new: true });
    return res.json({
        result: updated
    });
});

// PUSH
router.post('/:id/push', async (req, res) => {
    const { todo } = req.body;
    const updated = await todos.findOneAndUpdate({ _id: req.params.id, 'owner._id': req.user._id }, { $push: { items: todo }}, { new: true });
    return res.json({
        result: updated
    });
});

// PULL
router.post('/:id/pull', async (req, res) => {
    const { todo } = req.body;
    const updated = await todos.findOneAndUpdate({ _id: req.params.id, 'owner._id': req.user._id }, { $pull: { items: todo }}, { new: true });
    return res.json({
        result: updated
    });
});

// DELETE ALL
router.delete('/', async (req, res) => {
    const deleted = await todos.deleteMany({});
    return res.json({
        result: "OK"
    });
});

// DELETE
router.delete('/:id', async (req, res) => {
    const deleted = await todos.findOneAndDelete({ _id: req.params.id, 'owner._id': req.user._id });
    return res.json({
        result: "OK"
    });
});

module.exports = {
    router
}