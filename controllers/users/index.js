const router = require('express').Router();
const { middleware } = require('../auth-middleware');

router.get('/', middleware, (req, res) => {
    return res.json({ result: req.user });
});

module.exports = {
    router
}