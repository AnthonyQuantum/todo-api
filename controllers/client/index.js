const router = require('express').Router();
const request = require('request');
const { config } = require('../../config/index');

router.get('/', async (req, res, next) => {
    return res.redirect('/login');
});

router.get('/login', async (req, res, next) => {
    return res.render('login');
});

router.get('/register', async (req, res, next) => {
    return res.render('register');
});

router.post('/signin', async (req, res, next) => {
    var options = {
        url: config.BASE_URL + ':' + config.APP_PORT + '/api/auth/signin',
        method: 'POST',
        headers: {},
        form: req.body
    };
    request(options, (err, resp, body) => {
        if (!err && resp.statusCode == 200) {
            res.cookie('X-AUTH-TOKEN', resp.headers['x-auth-token']);
            return res.redirect('/todo');
        }
        return res.send(resp); //TODO: add error handling
    });
});

router.post('/signup', async (req, res, next) => {
    var options = {
        url: config.BASE_URL + ':' + config.APP_PORT + '/api/auth/signup',
        method: 'POST',
        headers: {},
        form: req.body
    };
    request(options, (err, resp, body) => {
        if (!err && resp.statusCode == 200) {
            res.cookie('X-AUTH-TOKEN', resp.headers['x-auth-token']);
            return res.redirect('/todo');
        }
        return res.send(resp); //TODO: add error handling
    });
});

router.get('/todo', async (req, res, next) => {
    var options = {
        url: config.BASE_URL + ':' + config.APP_PORT + '/api/todo',
        method: 'GET',
        headers: { 'X-AUTH-TOKEN': req.cookies['X-AUTH-TOKEN'] },
        qs: {}
    };
    request(options, (err, resp, body) => {
        if (!err && resp.statusCode == 200) {
            var list = JSON.parse(resp.body).result;
            return res.render('todo', {
                todolists: list
            });
            return res.json(list);
        }
        return res.send("Error"); //TODO: add error handling
    });
});

router.post('/pushitem/:id', async (req, res, next) => {
    var options = {
        url: config.BASE_URL + ':' + config.APP_PORT + '/api/todo/' + req.params.id + '/push',
        method: 'POST',
        headers: { 'X-AUTH-TOKEN': req.cookies['X-AUTH-TOKEN'] },
        form: req.body
    };
    request(options, (err, resp, body) => {
        if (!err && resp.statusCode == 200) {
            return res.redirect('/todo');
        }
        return res.send(resp); //TODO: add error handling
    });
});

router.post('/pullitem/:id', async (req, res, next) => {
    var options = {
        url: config.BASE_URL + ':' + config.APP_PORT + '/api/todo/' + req.params.id + '/pull',
        method: 'POST',
        headers: { 'X-AUTH-TOKEN': req.cookies['X-AUTH-TOKEN'] },
        form: req.body
    };
    request(options, (err, resp, body) => {
        if (!err && resp.statusCode == 200) {
            return res.redirect('/todo');
        }
        return res.send(resp); //TODO: add error handling
    });
});

router.post('/addlist', async (req, res, next) => {
    var options = {
        url: config.BASE_URL + ':' + config.APP_PORT + '/api/todo',
        method: 'POST',
        headers: { 'X-AUTH-TOKEN': req.cookies['X-AUTH-TOKEN'] },
        form: req.body
    };
    request(options, (err, resp, body) => {
        if (!err && resp.statusCode == 200) {
            return res.redirect('/todo');
        }
        return res.send(resp); //TODO: add error handling
    });
});

router.get('/deletelist/:id', async (req, res, next) => {
    var options = {
        url: config.BASE_URL + ':' + config.APP_PORT + '/api/todo/' + req.params.id,
        method: 'DELETE',
        headers: { 'X-AUTH-TOKEN': req.cookies['X-AUTH-TOKEN'] }
    };
    request(options, (err, resp, body) => {
        if (!err && resp.statusCode == 200) {
            return res.redirect('/todo');
        }
        return res.send(resp); //TODO: add error handling
    });
});


module.exports = {
    router
}