const { router: authRouter } = require('./auth/index');
const { router: userRouter } = require('./users/index');
const { router: todoRouter } = require('./todo/index');
const { router: clientRouter } = require('./client/index');

module.exports = {
    authRouter: authRouter,
    userRouter: userRouter,
    todoRouter: todoRouter,
    clientRouter: clientRouter
}