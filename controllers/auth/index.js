const router = require('express').Router();
const { userModel: users } = require('../../model/user');

router.post('/signup', async (req, res, next) => {
    const { login, password } = req.body;
    const foundUser = await users.findOne({ login: login });
    if (foundUser) {
        return res.json({
            err: {
                message: "The user with such login already exists"
            }
        })
    } else {
        const newUser = await users.create({ login, password });
        res.setHeader('X-AUTH-TOKEN', newUser._id);
        return res.json({
            result: "OK"
        });
    }
});

router.post('/signin', async (req, res, next) => {
    const { login, password } = req.body;
    const foundUser = await users.findOne({ login: login });
    if (!foundUser) {
       return res.json({
            err: {
                message: "User not found"
            }
        });
    } else {
        if (foundUser.password !== password) {
            return res.json({
                err: {
                    message: "Wrong password"
                }
            });
        }
        res.setHeader('X-AUTH-TOKEN', foundUser._id);
        return res.json({
            result: "OK"
        });
    }
});

module.exports = {
    router
};