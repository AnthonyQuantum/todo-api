const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const path = require('path');

const { config } = require('./config/index');
const { authRouter, userRouter, todoRouter, clientRouter } = require('./controllers/index');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static('views'))

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/api/auth', authRouter);
app.use('/api/user', userRouter);
app.use('/api/todo', todoRouter);
app.use('/', clientRouter);

mongoose.connect(config.DB_CONNECTION, (err) => {
    if (err) return console.log(err);

    app.listen(config.APP_PORT, (err) => {
        if (err) return console.log(err);
        console.log("Server running on port", config.APP_PORT);
    });
});
