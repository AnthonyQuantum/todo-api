const BASE_URL = 'http://localhost';
const APP_PORT = 3000;
const DB_CONNECTION = 'mongodb://localhost:27017/to-do';
const config = {
    APP_PORT,
    DB_CONNECTION,
    BASE_URL
};

module.exports = {
    config: config
};